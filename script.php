<?php
// Define en una constante la cantidad de filas y columnas de la matriz
define("TAMANO_MATRIZ", 3); // Puedes cambiar este valor según tus necesidades

// Función para generar una matriz cuadrada con números aleatorios entre 0 y 9
function generarMatriz($tamano) {
    $matriz = array();
    for ($i = 0; $i < $tamano; $i++) {
        $fila = array();
        for ($j = 0; $j < $tamano; $j++) {
            $fila[] = rand(0, 9);
        }
        $matriz[] = $fila;
    }
    return $matriz;
}

// Función para imprimir la matriz y calcular la suma de la diagonal principal
function imprimirMatrizYCalcularSuma($matriz) {
    $sumaDiagonal = 0;
    echo "<table border='1'>";
    for ($i = 0; $i < count($matriz); $i++) {
        echo "<tr>";
        for ($j = 0; $j < count($matriz[$i]); $j++) {
            echo "<td>{$matriz[$i][$j]}</td>";
            if ($i == $j) {
                $sumaDiagonal += $matriz[$i][$j];
            }
        }
        echo "</tr>";
    }
    echo "</table>";
    return $sumaDiagonal;
}

// Ciclo para generar matrices hasta que se cumpla la condición
do {
    $matrizGenerada = generarMatriz(TAMANO_MATRIZ);
    $sumaDiagonal = imprimirMatrizYCalcularSuma($matrizGenerada);

    if ($sumaDiagonal >= 10 && $sumaDiagonal <= 15) {
        echo "<p>La suma de la diagonal principal es $sumaDiagonal. Script finalizado.</p>";
        break;
    } else {
        echo "<p>La suma de la diagonal principal es $sumaDiagonal. Generando de vuelta otra matriz...</p>";
    }
} while (true);
?>
